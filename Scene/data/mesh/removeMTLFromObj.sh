#!/bin/bash

FILENAME="$1"

sed -i -e "/^mtl.*$/d" $FILENAME
sed -i -e "/^o.*$/d" $FILENAME
sed -i -e "/^s.*$/d" $FILENAME
sed -i -e "/^usemtl.*$/d" $FILENAME
