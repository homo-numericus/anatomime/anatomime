#version 120

uniform sampler2D backgroundTexture;
varying in vec2 texcoord;
void main (void)
{
	vec4 backgroundColor = texture2D(backgroundTexture,texcoord);

	gl_FragColor = backgroundColor ;
	gl_FragDepth = 1000000.0;
}
