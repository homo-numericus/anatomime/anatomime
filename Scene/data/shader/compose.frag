#version 120

uniform sampler2D contourPass;
uniform sampler2D bonesPass;
uniform sampler2D organsPass;
uniform sampler2D backgroundPass;
uniform sampler2D arteryPass;
uniform sampler2D musclesPass;

uniform float organsCoeff;
uniform float bonesCoeff;
uniform float arteryCoeff;
uniform float musclesCoeff;

void main (void)
{
	vec2 uv = gl_TexCoord[0].st; 
	vec2 uv_Inverted = gl_TexCoord[0].st;
	uv_Inverted.x = 1-uv_Inverted.x;

	vec4 contourColor = texture2D(contourPass, uv);
	vec4 bonesColor = texture2D(bonesPass, uv);
	vec4 organsColor = texture2D(organsPass, uv);
	vec4 arteryColor = texture2D(arteryPass, uv);
	vec4 musclesColor = texture2D(musclesPass, uv);
	vec4 backgroundColor = texture2D(backgroundPass, uv);

	vec4 finalColor = backgroundColor;

	float contoursCoeff = contourColor.x;

	vec4 insideColor = vec4(0.0,0.0,0.0,1.0);
	float currentSumCoeff = 0.0; //Skin ?

	//if(bonesCoeff < 1.0)
	{
		currentSumCoeff += musclesCoeff;
		insideColor += (musclesColor * musclesCoeff);

	//	if(musclesCoeff < 1.0)
		{
			currentSumCoeff += organsCoeff;
			insideColor  += (organsColor * organsCoeff);

	//		if(organsCoeff < 1.0)
			{
				currentSumCoeff += arteryCoeff;
				insideColor  += (arteryColor * arteryCoeff);
	//			if(arteryCoeff < 1.0)
				{
					currentSumCoeff += bonesCoeff;
					insideColor += (bonesColor * bonesCoeff);

				}
			}
		}
	}




	//insideColor  += (arteryColor * arteryCoeff)+(organsColor * organsCoeff);
	//insideColor /= (bonesCoeff + arteryCoeff + organsCoeff);

	vec3 merge = insideColor.xyz + (vec3(contoursCoeff,contoursCoeff,contoursCoeff));

	//if(bonesZ > 0.99999 && organsZ > 0.999999 )
	//	finalColor.xyz = backgroundColor.xyz + merge.xyz;
	//else
		finalColor.xyz = merge ;
	//finalColor.xyz = (1-contoursCoeff) * insideColor.xyz * finalColor.xyz;
	//finalColor.xyz = mix(mix(backgroundColor.xyz, insideColor.xyz,contoursCoeff), insideColor.xyz, 0.5);

	gl_FragColor = finalColor ;
}
