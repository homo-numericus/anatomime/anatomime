#version 120

////////
// Light parameters
uniform vec3 LightPosition;

// Planar mapping
uniform vec4 PlaneS, PlaneT;

varying vec3 Position;
varying vec3 Normal;
varying vec2 Texcoord;
varying vec3 Tangent;
varying vec3 Bitangent;
varying vec3 ViewDirection;
varying vec3 LightDirection;
///////

void main()
{
    gl_Position = ftransform();

    Position = gl_Vertex.xyz;
    Normal = gl_Normal;

#if defined(PlanarMapping)
    Texcoord.x = dot(vec4(gl_Vertex.xyz, 1.0), PlaneS);
    Texcoord.y = dot(vec4(gl_Vertex.xyz, 1.0), PlaneT);
#else
    Texcoord  = gl_MultiTexCoord0.xy;
#endif
#if defined(TexCoordFlipYAxis)
    Texcoord.y = 1 - Texcoord.y;
#endif
    Tangent   = gl_MultiTexCoord1.xyz;
    Bitangent = gl_MultiTexCoord2.xyz;

#if defined(Mirrored)
    Normal = -Normal;
    Tangent = -Tangent;
    Bitangent = -Bitangent;
#endif

    ViewDirection  = gl_ModelViewMatrixInverse[3].xyz - Position;
#if 0
    LightDirection = (gl_ModelViewMatrixInverse * vec4(LightPosition, 1)).xyz - Position;
#else
    LightDirection = LightPosition - Position;
#endif
}
